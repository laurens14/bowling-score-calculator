Calculates the score of a single bowling game given the frames (rounds) of a game.

Run tests in IDE of your choice  
or run using dotnet cli:    
`dotnet build && dotnet test --no-build -v=normal`
