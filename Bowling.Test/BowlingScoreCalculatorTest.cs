using System.IO;
using System.Linq;
using Bowling.Test.Model;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Bowling.Test
{
    public class Tests
    {
        [TestCase("gutter_game")]
        [TestCase("all_singles_game")]
        [TestCase("simple_spares_game")]
        [TestCase("single_strike_game")]
        [TestCase("multi_strike_game")]
        [TestCase("leadup_spare_game")]
        [TestCase("leadup_strike_game")]
        [TestCase("all_spares_game")]
        [TestCase("all_strikes_game")]
        public void CalculateGameScore(string testGameFile)
        {
            var testFile = File.ReadAllText($"resources/{testGameFile}.json");
            var testGame = JsonConvert.DeserializeObject<TestBowlingGame>(testFile);

            var frames = testGame.Frames.Select(r => new Frame(r)).ToList();
            var score = BowlingScoreCalculator.Calculate(frames);
            Assert.AreEqual(testGame.Result, score, $"{testGame.Name} game expects a result of {testGame.Result}");
        }
    }
}