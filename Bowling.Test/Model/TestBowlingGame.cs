using System.Collections;
using System.Collections.Generic;

namespace Bowling.Test.Model
{
    public class TestBowlingGame
    {
        public string Name { get; set; }
        public IEnumerable<IEnumerable<int>> Frames { get; set; }
        public int Result { get; set; }
    }
}