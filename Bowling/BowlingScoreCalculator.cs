using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Bowling
{
    public static class BowlingScoreCalculator
    {
        /// <summary>
        /// Calculates the total score for a single game of bowling
        /// </summary>
        /// <param name="frames">Frames containing the pin's hit per throw</param>
        /// <returns>Total score</returns>
        public static int Calculate(IList<Frame> frames)
        {
            return frames.Select((frame, i) =>
            {
                if (frame.IsStrike) return frame.Pins.Sum() + GetNextThrows(frames, i, 2).Sum();
                if (frame.IsSpare) return frame.Pins.Sum() + GetNextThrows(frames, i, 1).Sum();
     
                return frame.Pins.Sum();
            }).Sum();
        }

        /// <summary>
        /// Gets the next x amount of throws starting from currentFrameIndex
        /// </summary>
        /// <param name="frames">all frames for a single game</param>
        /// <param name="currentFrameIndex">index of current frame in frames</param>
        /// <param name="amount">amount of throws to return</param>
        /// <returns>x amount of throws after the current frame's throws</returns>
        private static IEnumerable<int> GetNextThrows(IEnumerable<Frame> frames, int currentFrameIndex, int amount)
        {
            return frames.Skip(currentFrameIndex + 1).SelectMany(t => t.Pins).Take(amount);
        }    
    }
    
    
    // In bowling, a round is called a frame
    public class Frame
    {
        //IEnumerable of this frame's pins hit per throw
        public IEnumerable<int> Pins { get; }

        public Frame(IEnumerable<int> pins)
        {
            this.Pins = pins;
        }

        public bool IsStrike => Pins.Count() == 1 && Pins.Sum() == 10;
        public bool IsSpare => Pins.Count() == 2 && Pins.Sum() == 10;
    }
}